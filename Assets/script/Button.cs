﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public bool Reusable = false;

    private bool animationIsPlaying = false;

    public delegate void OnButtonPress();

    public event OnButtonPress OnButtonPressEvent;

    private Animator animator;

    private new AudioSource audio;

    private Game game;
    

    void Start()
    {
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        game = transform.root.GetComponent<Game>();
    }

    void Update()
    {
        
    }

    public void SetActive(bool state)
    {
        GetComponent<Collider>().enabled = state;
    }

    public void Press()
    {
        //if (animationIsPlaying)
        //    return;
        if (audio)
        {
            audio.Play();
        }
        //animationIsPlaying = true;
        //if (!Reusable) {
        //    animator.Play("press");
        //    SetActive(false);
        //}
        //else {
        //    animator.Play("press_release");
        //}

        game.OnButtonPress(this);
        
        OnButtonPressEvent?.Invoke();
    }

    public void OnPressAnimEnd()
    {
        animationIsPlaying = false;
    }
}
