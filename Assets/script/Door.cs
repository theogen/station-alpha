﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Open(bool immediately = false)
    {
        if (!immediately)
            animator.Play("open");
        else
            animator.Play("open", 0, 1f);
    }

    public void Close(bool immediately)
    {
        if (!immediately)
            animator.Play("close");
        else
            animator.Play("close", 0, 1f);
    }
}
