﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using TMPro;

public class Game : MonoBehaviour
{
    public InputActionReference PauseAction;
    public InputActionReference DialogueAdvanceAction;
    
    public Train Train;
    public WorldGenerator Generator;

    public Material FinalStationMaterial;
    public string FinalStationName;

    public static UI UI;

    public static Game game;

    bool dialogueActive = true;
    GameObject dialogueParent;
    Text dialogueText;

    public Player player { get; private set; }

    int firstDialogue = 0;
    int trainEnteredTimes = 0;
    int stationEnteredTimes = 0;

    bool loopBroken = false;

    bool canPullBrake = false;

    bool canPullBrakeDialogue = false;

    bool canTakeTrain = true;

    bool pause = true;

    void Start()
    {
        game = this;
        player = transform.Find("player").GetComponent<Player>();
        DialogueAdvanceAction.action.started += OnDialogueAdvance;

        PauseAction.action.started += OnPause;
        PauseAction.action.Enable();

        dialogueParent = transform.Find("dialogue").gameObject;
        dialogueText = dialogueParent.transform.Find("bg/text").GetComponent<Text>();

        if (SceneManager.sceneCount == 1)
        {
            SceneManager.LoadScene("ui", LoadSceneMode.Additive);
            PlayerPause(true);
        }
        else
        {
            UI.UpdateSettings();
            StartGame();
        }
    }

    private void OnDestroy()
    {
        PauseAction.action.started -= OnPause;
        DialogueAdvanceAction.action.started -= OnDialogueAdvance;
    }

    public void StartGame()
    {
        SetDialogueActive(true);
        player.Controller.CursorLocked = true;
        pause = false;

        Train.Move(2000, 100f, 0f, delegate() {
            Train.OpenDoors(DoorsSide.Front);
            transform.Find("world/station/station_enter").GetComponent<Collider>().isTrigger = true;
            Train.moveWorld = true;
        });
    }

    void PlayerPause(bool state)
    {
        player.Controller.Active = !state;
        player.Controller.CursorLocked = !state;
        player.Controller.CrosshairActive = !state;
        if (state)
            player.UseAction.action.Disable();
        else
            player.UseAction.action.Enable();
    }

    public void OnPause(InputAction.CallbackContext obj)
    {
        pause = !pause;
        UI.PauseMenu(pause);
        UI.ReplaceMainMenuWithPauseMenu();
        player.Controller.CursorLocked = !pause;
        if (dialogueActive)
        {
            dialogueParent.gameObject.SetActive(!pause);
            if (pause)
                DialogueAdvanceAction.action.Disable();
            else
                DialogueAdvanceAction.action.Enable();
        }
        else
        {
            PlayerPause(pause);
        }
    }

    public void ResumeGame()
    {
        pause = false;
        player.Controller.CursorLocked = true;
        if (dialogueActive)
        {
            dialogueParent.gameObject.SetActive(true);
            DialogueAdvanceAction.action.Enable();
        }
        else
        {
            PlayerPause(false);
        }
    }

    private void Update()
    {
    }

    void SetDialogueActive(bool state)
    {
        dialogueParent.gameObject.SetActive(state);
        dialogueActive = state;
        if (state)
        {
            DialogueAdvanceAction.action.Enable();
            player.UseAction.action.Disable();
        }
        else
        {
            DialogueAdvanceAction.action.Disable();
            player.UseAction.action.Enable();
        }
        player.Controller.Active = !state;
        player.Controller.CrosshairActive = !state;
    }

    void OnDialogueAdvance(InputAction.CallbackContext obj)
    {
        if (firstDialogue == 0)
        {
            ++firstDialogue;
        }
        SetDialogueActive(false);
    }

    public void OnTrainEnter()
    {
        if (Train.IsMoving || Train.Stopped)
            return;
        Train.CloseDoors(DoorsSide.Front);
        float distance = WorldGenerator.ModuleLength * (Generator.StationsDistance + 1);
        Train.Move(distance, 100f, 3f, delegate {
            Train.OpenDoors(DoorsSide.Front);
            transform.Find("welcome_alpha").GetComponent<AudioSource>().Play();
        });
        ++trainEnteredTimes;
    }

    public void OnStationEnter()
    {
        if (trainEnteredTimes == 0)
            return;
        if (stationEnteredTimes == 0)
        {
            dialogueText.text = "Wait... What? I was at this same station just earlier.\n\nOr is my memory failing me?";
            SetDialogueActive(true);
            ++stationEnteredTimes;
            return;
        }
        if (trainEnteredTimes > 1 && stationEnteredTimes == 1)
        {
            dialogueText.text = "Something is definitely not right. What's going on?";
            SetDialogueActive(true);
            ++stationEnteredTimes;
            canPullBrake = true;
            return;
        }
        if (stationEnteredTimes == 2 && loopBroken)
        {
            transform.Find("welcome_beta").GetComponent<AudioSource>().Play();
            dialogueText.text = "Finally! My home station!\n\n(Thanks for playing! This is the end.)";
            SetDialogueActive(true);
            ++stationEnteredTimes;
            return;
        }
    }

    public void SetStation(string name, Material wallsMaterial)
    {
        Transform station = transform.Find("world/station");
        const string welcomeText = "WELCOME TO STATION ";
        station.Find("station_name_1").GetComponent<TextMeshPro>().text = welcomeText + name;
        station.Find("station_name_2").GetComponent<TextMeshPro>().text = welcomeText + name;
        Transform walls = station.Find("walls/inside");
        foreach (Transform wall in walls)
        {
            wall.GetComponent<MeshRenderer>().material = wallsMaterial;
            var tilingController = wall.GetComponent<TextureTilingController>();
            tilingController.texture = wallsMaterial.mainTexture;
            tilingController.UpdateTiling();
        }
    }

    public void TriggerEntered(string name)
    {
        if (name == "station_enter")
        {
            OnStationEnter();
            canTakeTrain = true;
        }
        if (name == "station_exit" && !loopBroken)
        {
            dialogueText.text = "Why would I want to go back?";
            if (stationEnteredTimes > 0)
                dialogueText.text = "Some invisible force is pulling me back!";
            SetDialogueActive(true);
        }
        if (name == "train_enter" && canTakeTrain)
        {
            OnTrainEnter();
            canTakeTrain = false;
        }
    }

    public void TriggerExited(string name)
    {
        
    }

    public void OnButtonPress(Button button)
    {
        if (button.name == "emergency_brake_zone")
        {
            if (stationEnteredTimes < 1)
            {
                dialogueText.text = "Why in the world would I do that?";
                SetDialogueActive(true);
            }

            if (!canPullBrake)
                return;

            if (!canPullBrakeDialogue)
            {
                dialogueText.text = "Well... the train is empty anyway... and it doesn't seem like I have much to lose.\n\nI would do anything now to get out of this ridiculous loop.";
                SetDialogueActive(true);
                canPullBrakeDialogue = true;
                return;
            }

            if (Train.CurrentSpeed > 100f)
            {
                if (Train.Stopped)
                    return;
                button.transform.parent.GetComponent<Animator>().Play("pull");
                Train.Stop(delegate() {
                    Train.OpenDoors(DoorsSide.Back & DoorsSide.Front);
                });
                SetStation(FinalStationName, FinalStationMaterial);
                loopBroken = true;
                Destroy(transform.Find("world/station/rail_walls").gameObject);
            }
            else
            {
                dialogueText.text = "It doesn't pull... Let me try again.";
                SetDialogueActive(true);
            }
        }
    }
}
