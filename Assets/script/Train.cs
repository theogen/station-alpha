﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DoorsSide
{
    Front = 1,
    Back = 2
}

public class Train : MonoBehaviour
{
    List<Door> doorsFront = new List<Door>();
    List<Door> doorsBack = new List<Door>();

    private static float MoveVolumeMultiplier = 3f;

    public bool IsMoving { get; private set; }
    public bool Stopped { get; set; }

    public float CurrentSpeed { get; private set; }

    Coroutine movingCoroutine = null;

    AudioSource moveAudio;
    AudioSource stopAudio;
    AudioSource doorAudio;

    public bool moveWorld = false;

    float previousZ;

    private Transform world;

    void Start()
    {
        foreach (Transform car in transform)
        {
            if (!car.name.StartsWith("car_"))
                continue;
            Transform doorsParent = car.Find("doors");
            doorsFront.Add(doorsParent.Find("1").GetComponent<Door>());
            doorsFront.Add(doorsParent.Find("2").GetComponent<Door>());
            doorsFront.Add(doorsParent.Find("3").GetComponent<Door>());
            doorsBack.Add(doorsParent.Find("4").GetComponent<Door>());
            doorsBack.Add(doorsParent.Find("5").GetComponent<Door>());
            doorsBack.Add(doorsParent.Find("6").GetComponent<Door>());
        }

        var audios = GetComponents<AudioSource>();
        moveAudio = audios[0];
        stopAudio = audios[1];
        previousZ = transform.position.z;
        doorAudio = transform.Find("door_sound").GetComponent<AudioSource>();

        world = transform.root.Find("world");
    }

    private void Update()
    {
        //if (CurrentSpeed > 0f)
        //    audio.volume += CurrentSpeed / 1000f * Time.deltaTime;
        //if (CurrentSpeed > 100f && audio.pitch < 1.3f)
        //    audio.pitch += CurrentSpeed * Time.deltaTime / 1000;
        //if (CurrentSpeed < 100f && audio.pitch > 1f)
        //    audio.pitch -= CurrentSpeed * Time.deltaTime / 1000;
        //if (currentSpeedPrev > CurrentSpeed && CurrentSpeed < 100f)
        //    audio.volume -= CurrentSpeed / 1000f * Time.deltaTime;

        if (!moveWorld)
        {
            CurrentSpeed = (transform.localPosition.z - previousZ) / Time.deltaTime;
            previousZ = transform.localPosition.z;
        }
        else
        {
            CurrentSpeed = Mathf.Abs((world.localPosition.z - previousZ) / Time.deltaTime);
            previousZ = world.localPosition.z;
        }
    }

    public void OpenDoors(DoorsSide side, bool immediately = false)
    {
        if ((side | DoorsSide.Front) == DoorsSide.Front)
            foreach (Door door in doorsFront)
                door.Open(immediately);
        if ((side | DoorsSide.Back) == DoorsSide.Back)
            foreach (Door door in doorsBack)
                door.Open(immediately);
        if (!immediately)
            doorAudio.Play();
    }

    public void CloseDoors(DoorsSide side, bool immediately = false)
    {
        foreach (Door door in doorsFront)
            door.Close(immediately);
        if (!immediately)
            doorAudio.Play();
    }

    public void Move(float distance, float speed, float waitBefore = 0f, AnimationHelper.WhenDoneFunction onEnd = null, AnimationHelper.EasingFunction easing = null)
    {
        IsMoving = true;
        float time = distance / speed;
        Transform obj = transform;
        if (moveWorld)
            obj = world;
        Vector3 initPos = obj.localPosition;
        Vector3 distanceV = Vector3.forward * distance;
        Vector3 newPos;
        AnimationHelper.EasingFunction useEasing = AnimationHelper.EaseInOut;
        if (easing != null)
            useEasing = easing;
        moveAudio.Play();
        movingCoroutine = StartCoroutine(AnimationHelper.RunAnimation(time, delegate(float timePerc, float secondsPassed) {
            if (!moveWorld)
                newPos = initPos + distanceV * timePerc;
            else
                newPos = initPos - distanceV * timePerc;
            obj.localPosition = newPos;
            if (timePerc < 0.5f)
                moveAudio.volume = timePerc * 2f * MoveVolumeMultiplier * (UI.sfxVolume / 100f);
            else
                moveAudio.volume = (1f - timePerc) * 2f * MoveVolumeMultiplier * (UI.sfxVolume / 100f);
        }, useEasing, AnimationHelper.SquareFunction, delegate() {
            if (!moveWorld)
                obj.localPosition = initPos + distanceV;
            else
            {
                obj.localPosition = Vector3.zero;
                foreach (Transform child in obj)
                    child.localPosition -= distanceV;
            }
            IsMoving = false;
            movingCoroutine = null;
            CurrentSpeed = 0f;
            moveAudio.volume = 0f;
            onEnd?.Invoke();
        }, 0f, waitBefore));
    }

    public void Stop(AnimationHelper.WhenDoneFunction onEnd = null)
    {
        stopAudio.Play();
        StopCoroutine(movingCoroutine);
        if (moveWorld)
        {
            foreach (Transform child in world)
                child.localPosition += Vector3.forward * world.localPosition.z;
            world.localPosition = Vector3.zero;
        }
        movingCoroutine = null;
        Move(800, CurrentSpeed, 0f, onEnd, AnimationHelper.EaseOut);
        Stopped = true;
    }
}
