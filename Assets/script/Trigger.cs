﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player")
            return;

        transform.root.GetComponent<Game>().TriggerEntered(name);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag != "Player")
            return;

        transform.root.GetComponent<Game>().TriggerExited(name);
    }
}
