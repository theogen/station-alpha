﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButton : MonoBehaviour
{
    private const float PopDistance = 20f;

    public delegate void OnButtonPress();

    public event OnButtonPress OnButtonPressEvent;

    public Color HighlightColor;
    public Color PressColor;
    public Color SelectColor;


    AudioSource clickSound;

    Color normalColor;

    new BoxCollider2D collider;
    RectTransform rectt;
    Image image;

    private bool initialized = false;

    void Start()
    {
        collider = GetComponent<BoxCollider2D>();
        rectt = GetComponent<RectTransform>();
        image = GetComponent<Image>();

        collider.size = rectt.rect.size;
        collider.offset = rectt.rect.center;

        normalColor = image.color;

        clickSound = GameObject.Find("ui_click").GetComponent<AudioSource>();

        initialized = true;
    }

    void OnDisable()
    {
        if (!initialized)
            return;
        image.color = normalColor;
    }

    void OnMouseEnter()
    {
        image.color = HighlightColor;
    }

    void OnMouseExit()
    {
        image.color = normalColor;
    }

    void OnMouseDown()
    {
        image.color = PressColor;
    }

    void OnMouseUp()
    {
        image.color = HighlightColor;
    }

    void OnMouseUpAsButton()
    {
        clickSound.Play();
        OnButtonPressEvent?.Invoke();
    }
}
