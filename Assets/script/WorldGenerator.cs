﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerator : MonoBehaviour
{
    public GameObject StationPrefab;
    public GameObject TunnelPrefab;

    public GameObject Player;

    [Range(1, 10)]
    public int StationsDistance = 5;

    public const float ModuleLength = 400f;

    float stationsDistanceMeters;

    GameObject stationPrev;
    GameObject stationCurrent;
    GameObject stationNext;

    List<GameObject> tunnelsBack = new List<GameObject>();
    List<GameObject> tunnelsForward = new List<GameObject>();

    void Start()
    {
        stationsDistanceMeters = StationsDistance * ModuleLength;
        stationCurrent = transform.GetChild(0).gameObject;

        for (int i = 1; i <= StationsDistance; ++i)
        {
            GameObject tunnel = Instantiate(TunnelPrefab, transform);
            tunnel.transform.position = new Vector3(0f, 0f, ModuleLength * i);
            tunnelsForward.Add(tunnel);
            tunnel.name = "a" + i;
        }

        for (int i = 1; i <= StationsDistance; ++i)
        {
            GameObject tunnel = Instantiate(TunnelPrefab, transform);
            tunnel.transform.position = new Vector3(0f, 0f, ModuleLength * -i);
            tunnelsBack.Add(tunnel);
            tunnel.name = "b" + i;
        }

        //stationNext = Instantiate(StationPrefab, transform);
        //stationNext.transform.position = new Vector3(0f, 0f, ModuleLength * (StationsDistance + 1));
        //stationPrev= Instantiate(StationPrefab, transform);
        //stationPrev.transform.position = new Vector3(0f, 0f, ModuleLength * -(StationsDistance + 1));
    }

    void Shift(bool backwards = false)
    {
        Vector3 tunnelMove = new Vector3(0f, 0f, stationsDistanceMeters * 2 + ModuleLength * 2);
        Vector3 stationMove = new Vector3(0f, 0f, stationsDistanceMeters + ModuleLength);
        List<GameObject> tunnels = tunnelsBack;
        if (backwards)
        {
            tunnelMove = -tunnelMove;
            stationMove = -stationMove;
            tunnels = tunnelsForward;
        }
        foreach (GameObject tunnel in tunnels)
        {
            tunnel.transform.localPosition += tunnelMove;
        }
        stationCurrent.transform.localPosition += stationMove;

        if (!backwards)
        {
            List<GameObject> tmp = tunnelsBack;
            tunnelsBack = tunnelsForward;
            tunnelsForward = tmp;
        }
        else
        {
            List<GameObject> tmp = tunnelsForward;
            tunnelsForward = tunnelsBack;
            tunnelsBack = tmp;
        }
    }

    void ShiftBackward()
    {

    }

    private void Update()
    {
        if (Player.transform.position.z > stationCurrent.transform.position.z + stationsDistanceMeters / 2)
            Shift();
        if (Player.transform.position.z < stationCurrent.transform.position.z - stationsDistanceMeters / 2)
            Shift(true);
    }
}
